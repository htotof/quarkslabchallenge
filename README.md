# Challenge Quarkslab

- Contact: jmmartinez@quarkslab.com
- Date: mer. 19 sept. 2018

## The machine

This is an informal design for a stack interpreter that manipulates 32 bit signed integers and understands a few instructions instructions.

In the following TOP denotes the element on top of the stack, and SECOND the second element on top of the stack.

Each instruction has an index in the code, starting from 0, and an optional
integer argument, as in ``3 POP 2`` which is the forth instruction of the
listing, named ``POP`` with a single argument 2.

### Instructions

READ: 
reads an integer on stdin and push the value on the stack, or exit if input is  invalid

WRITE: 
pops the top value of the stack, and prints it on stdout

DUP: 
duplicate the value on top of the stack

MUL: 
pops the two top value of the stack, multiply them and push the result on top of the stack

ADD: 
pops the two top value of the stack, add them and push the result on top of the stack

SUB: 
pops the two top value of the stack, sub the TOP with the SECOND and push the result on the stack

GT: 
LT:
EQ:
pops the two top values from the stack, compare them for TOP > SECOND, TOP < SECOND or TOP == SECOND and push the result as 0 or 1 on the stack

JMPZ:
pops the two top value of the stack. Jump to the <n>th instruction, where <n> was the first value on the stack, if the top value is null. Otherwise just drop these two values

PUSH \<n\>:
push the integer value <n> on the stack

POP \<n\>:
pop n value from the stack

ROT \<n\>:
perform a circular rotation on the first n value of the stack toward the top
for instance the stack : BOTTOM [1,2,4,8] TOP becomes BOTTOM [1, 8, 2, 4] TOP after ROT 3

### Example

The following program reads a number and prints its value squared if the value is positive or null, and 0 otherwise

```text
0 READ
1 DUP
2 PUSH 0
3 GT
4 PUSH 8
5 JMPZ
6 POP 1
7 PUSH 0
8 DUP
9 MUL
10 WRITE
```

### Exercise

Using this language, write an implementation of the factorial function that reads a number from stdin and outputs its result on stdout. 

Provide an interpreter that can interpret your code, written in C++.

It's not mandatory to write the parser for the assembly, a global array will also be fine.

## Project details 

### Design note

This project has been designed to make it easy to add OpCode:

1- Create class inherite from `AbstractOpCode` => you had to implemente the method 
```c++
void do_apply(ExecutionContext &ctx, OptionalParam param) override;`
```

2- Write `.cc` implementation. Add the MACRO

> REGISTER_OPCODE

it register OpCode to be supported... Nothing more to do to add new OpCode support.

### dev. Env.

| Tools | Name | Version |
|---|---|---|
| Compiler | clang | 6.0.1 (tags/RELEASE_601/final) | 
| Source manager | git | 2.18.0 |
| Tool Chain Maker | cmake | 3.12.1 |

### Setup

1- Clone file fron repo

```bash
git clone quarkslab .
cd quarkslab
```

2- Update submodule

> git submodule update --init --recursive

3- Build project

```bash
mkdir build
cd $_
cmake ..
cmake --build .
```

#### Execute Unit Test

> ctest

#### Check source code

- With clang-tidy (external dependencies)

> cmake --build . --target 

#### Format source code

- With clang-format (external dependencies)

> make clangformat