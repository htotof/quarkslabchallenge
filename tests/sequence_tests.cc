/*!
 * @file sequence_opcode_test.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * OpCode sequence Test Case
 */

#include "catch2/catch.hpp"
#include "helpers.h"
#include "helpers/convert.h"
#include "opcode/opcode.h"
#include "opcode/opcode_factory_mgr.h"
#include <iostream>
#include <sstream>

using namespace opcode;

namespace /*anonymous*/ {
bool script_player(std::istringstream &input) {
  using helpers::get_opcode_from_string;
  using opcode::ExecutionContext;
  using opcode::OpCodeFactoryMgr;

  ExecutionContext execution_ctx;

  // Convert
  for (std::string line; std::getline(input, line);) {
    try {
      // Translate the readed line
      uint32_t cur_idx;
      std::string opcode;
      std::pair<bool, int32_t> parameter;
      std::tie(cur_idx, opcode, parameter) = get_opcode_from_string(line);

      // First check
      if (cur_idx != execution_ctx.next_opcode_idx) {
        continue;
      }

      // Valid index command
      ++execution_ctx.next_opcode_idx;

      // Get opcode object
      auto opcode_obj =
          OpCodeFactoryMgr::instance().get_opcode_factory().get_opcode(opcode);

      // Execute code
      opcode_obj->apply(execution_ctx, parameter);

    } catch (opcode::StopExecution) {
      return true;
    } catch (...) {
      return false;
    }
  }
  return true;
}

} // namespace

// Valid Test Case of all OpCode
TEST_CASE("Squared test case", "[SEQUENCE]") {
  std::string script{"0 READ\n1 DUP\n2 PUSH 0\n3 GT\n4 PUSH 8\n5 JMPZ"
                     "\n6 POP 1\n7 PUSH 0\n8 DUP\n9 MUL\n10 WRITE\n"};

  SECTION("Input is positive") {
    std::string give_number("5");
    std::istringstream in(script);
    std::istringstream std_in(give_number);
    std::ostringstream std_out;

    // Redirect stdin and stdout to
    redirect_std_stream(&std_out, &std_in);

    try {
      REQUIRE(script_player(in));
      restore_std_stream();
      REQUIRE(std_out.str() == "Get Number (+/-) => 25\n");
    } catch (...) {
      restore_std_stream();
      REQUIRE(false);
    }
  } // #--- End of section

  SECTION("Input is null") {
    std::string give_number("0");
    std::istringstream in(script);
    std::istringstream std_in(give_number);
    std::ostringstream std_out;

    // Redirect stdin and stdout to
    redirect_std_stream(&std_out, &std_in);

    try {
      REQUIRE(script_player(in));
      restore_std_stream();
      REQUIRE(std_out.str() == "Get Number (+/-) => 0\n");
    } catch (...) {
      restore_std_stream();
      REQUIRE(false);
    }
  } // #--- End of section

  SECTION("Input is negative") {
    std::string give_number("-1");
    std::istringstream in(script);
    std::istringstream std_in(give_number);
    std::ostringstream std_out;

    // Redirect stdin and stdout to
    redirect_std_stream(&std_out, &std_in);
    try {
      REQUIRE(script_player(in));
      restore_std_stream();
      REQUIRE(std_out.str() == "Get Number (+/-) => 0\n");
    } catch (...) {
      restore_std_stream();
      REQUIRE(false);
    }
  } // #--- End of section
} // #--- End of test case
