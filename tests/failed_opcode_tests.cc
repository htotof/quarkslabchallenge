/*!
 * @file failed_opcode_test.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Unit test case that highlight bug
 */

#include <iostream>
#include <sstream>
#include "catch2/catch.hpp"
#include "helpers.h"
#include "opcode/add.h"
#include "opcode/dup.h"
#include "opcode/eq.h"
#include "opcode/gt.h"
#include "opcode/jmpz.h"
#include "opcode/lt.h"
#include "opcode/mul.h"
#include "opcode/opcode.h"
#include "opcode/pop.h"
#include "opcode/push.h"
#include "opcode/read.h"
#include "opcode/rot.h"
#include "opcode/sub.h"
#include "opcode/write.h"

using namespace opcode;

namespace /*anonymous*/ {

#define TEST_OPCODE_TOO_SHORT_STACK(oc) \
  opcode::oc##OpCode opcode;            \
  opcode::ExecutionContext ctx;         \
  ctx.stack.push(2);                    \
  REQUIRE_THROWS(opcode.apply(ctx));

}  // namespace

// Invalid Test Case of all OpCode
TEST_CASE("InvalidTest", "[OPCODE]") {
  SECTION("Empty Stack for WRITE OpCode") {
    opcode::WriteOpCode write_op;
    opcode::ExecutionContext ctx;

    try {
      write_op.apply(ctx);
      REQUIRE(false);
    } catch (std::exception&) {
      REQUIRE(true);
    } catch (...) {
      REQUIRE(false);
    }
  }
  // #--- End of WRITE OpCode Test

  SECTION("Get other value that an integer to READ") {
    opcode::ReadOpCode read_op;
    opcode::ExecutionContext ctx;

    // Replace and prepare input
    std::string give_input("toto on the beach");
    std::istringstream in(give_input);
    std::ostringstream out;
    redirect_std_stream(&out, &in);

    try {
      // Execute test
      read_op.apply(ctx);
      REQUIRE(false);
    } catch (...) {
      // Restaure previous status
      restore_std_stream();
      REQUIRE(true);
    }
  }
  // #--- End of READ OpCode Test

  SECTION("Use too short stack for MUL OpCode") {
    TEST_OPCODE_TOO_SHORT_STACK(Mul);
  }
  // #--- End of MUL Test Case
  SECTION("Use too short stack for ADD OpCode") {
    TEST_OPCODE_TOO_SHORT_STACK(Add);
  }
  // #--- End of ADD Test Case
  SECTION("Use too short stack for SUB OpCode") {
    TEST_OPCODE_TOO_SHORT_STACK(Sub);
  }
  // #--- End of SUB Test Case
  SECTION("Use GT OpCode") { TEST_OPCODE_TOO_SHORT_STACK(Gt); }
  // #--- End of GT Test Case
  SECTION("Use LT OpCode") { TEST_OPCODE_TOO_SHORT_STACK(Lt); }
  // #--- End of LT Test Case
  SECTION("Use EQ OpCode") { TEST_OPCODE_TOO_SHORT_STACK(Eq); }
  // #--- End of EQ Test Case
  SECTION("Use JMPZ OpCode") { REQUIRE(false); }
  // #--- End of JMPZ Test Case
  SECTION("Use PUSH OpCode") { REQUIRE(false); }
  // #--- End of PUSH Test Case
  SECTION("Use POP OpCode") { REQUIRE(false); }
  // #--- End of POP Test Case
  SECTION("Use ROT OpCode") { REQUIRE(false); }
  // #--- End of ROT Test Case
}  //#--- End of Invalid Test Case
