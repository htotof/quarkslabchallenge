/*!
 * @file helpers.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Common code use to tests case
 */
#ifndef TESTS_HELPERS_H
#define TESTS_HELPERS_H

#include <iostream>

/*!
 * Allow to save standard input and ouput and replace it by controled streame
 *
 * @param [in] out ouput streamer substitute for std::cout
 * @param [in] in ouput streamer substitute for std::in
 */
void redirect_std_stream(std::ostringstream *out = nullptr,
                     std::istringstream *in = nullptr);

// Restore std::cout and std::cin
void restore_std_stream();

#endif  // TESTS_HELPERS_H
