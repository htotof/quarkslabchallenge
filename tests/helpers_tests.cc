/*!
 * @file helpers_test.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Helpers functions test case
 */

// this in one cpp file
#include <experimental/filesystem>
#include <list>
#include <string>
#include <tuple>
#include "catch2/catch.hpp"
#include "helpers/convert.h"

// To adapt C++20 filesystem integration more easier
namespace fs = std::experimental::filesystem;

TEST_CASE("StringAnalyze", "[HELPERS]") {
  using namespace helpers;
  uint32_t cur_idx;
  std::string opcode;
  std::pair<bool, uint32_t> parameter;

  SECTION("Full OpCode string (Index OpCode Param)") {
    std::tie(cur_idx, opcode, parameter) = get_opcode_from_string("2 PUSH 0");
    REQUIRE(cur_idx == 2);
    REQUIRE(opcode == "PUSH");
    REQUIRE(parameter.first);
    REQUIRE(parameter.second == 0);
  }

  SECTION("Full OpCode string with negative parameter (Index OpCode Param)") {
    std::tie(cur_idx, opcode, parameter) = get_opcode_from_string("2 PUSH -5");
    REQUIRE(cur_idx == 2);
    REQUIRE(opcode == "PUSH");
    REQUIRE(parameter.first);
    REQUIRE(parameter.second == -5);
  }

  SECTION("Full OpCode string with negative parameter and big index") {
    std::tie(cur_idx, opcode, parameter) = get_opcode_from_string("42 ROT -2");
    REQUIRE(cur_idx == 42);
    REQUIRE(opcode == "ROT");
    REQUIRE(parameter.first);
    REQUIRE(parameter.second == -2);
  }

  SECTION("Full OpCode string with great negative parameter") {
    std::tie(cur_idx, opcode, parameter) = get_opcode_from_string("0 MUL -666");
    REQUIRE(cur_idx == 0);
    REQUIRE(opcode == "MUL");
    REQUIRE(parameter.first);
    REQUIRE(parameter.second == -666);
  }

  SECTION("OpCode without parameter") {
    std::tie(cur_idx, opcode, parameter) = get_opcode_from_string("1 DUP");
    REQUIRE(cur_idx == 1);
    REQUIRE(opcode == "DUP");
    REQUIRE(parameter.first == false);
  }

  SECTION("Big OpCode") {
    std::tie(cur_idx, opcode, parameter) =
        get_opcode_from_string("5 JMPZBUTMAYBE");
    REQUIRE(cur_idx == 5);
    REQUIRE(opcode == "JMPZBUTMAYBE");
    REQUIRE(parameter.first == false);
  }

  SECTION("Inavlid OpCode String") {
    REQUIRE_THROWS(get_opcode_from_string("5 45 TOTO"));
  }

}  //#--- End of test ReadFileTest
