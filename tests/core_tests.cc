/*!
 * @file core_test.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Core Unit Test
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do
                           // this in one cpp file
#include <experimental/filesystem>
#include <list>
#include <string>
#include "catch2/catch.hpp"
#include "opcode/opcode.h"
#include "opcode/opcode_factory.h"
#include "opcode/opcode_factory_mgr.h"

// To adapt C++20 filesystem integration more easier
namespace fs = std::experimental::filesystem;

using namespace opcode;

TEST_CASE("RegisterOpCode", "[CORE]") {
  OpCodeFactory factory;

  class FakeOpCode : public AbstractOpCode {
   public:
    virtual void do_apply(ExecutionContext &ctx, OptionalParam param) {
      // Avoid clang++ flag 'unused_paremeters'
      if (false == param.first) {
        ctx.stack.push(1);
      } else {
        ctx.stack.push(1);
      }
    }
  };

  factory.register_opcode("TOTO", std::make_shared<FakeOpCode>());
  auto opcode = factory.get_opcode("TOTO");

  opcode::ExecutionContext ctx;
  opcode->apply(ctx);
  REQUIRE(false == ctx.stack.empty());
}  //#--- End of test RegisterOpCode

TEST_CASE("CheckRegisterOpCode", "[CORE]") {
  std::list<std::string> available_opcode{"READ", "WRITE", "DUP", "MUL",
                                          "SUB",  "GT",    "LT",  "EQ",
                                          "JMPZ", "PUSH",  "POP", "ROT"};

  for (const auto &opcode : available_opcode) {
    try {
      auto opcode_obj =
          OpCodeFactoryMgr::instance().get_opcode_factory().get_opcode(opcode);
    } catch (...) {
      REQUIRE(opcode == "is missing");
    }
  }
  REQUIRE(true);
}
// #--- End of test case Registerd OpCode check
