#include "helpers.h"
#include <sstream>

namespace /*anonymous*/ {
static std::streambuf *cout_buf = nullptr;
static std::streambuf *cin_buf = nullptr;
}  // namespace
// #--- End of anonymous

// Save current std::cout and std::cin to be replace by parameters
void redirect_std_stream(std::ostringstream *out, std::istringstream *in) {
  if (out) {
    cout_buf = std::cout.rdbuf();
    std::cout.rdbuf(out->rdbuf());
  } else {
    cout_buf = nullptr;
  }

  if (in) {
    cin_buf = std::cin.rdbuf();
    std::cin.rdbuf(in->rdbuf());
  } else {
    cin_buf = nullptr;
  }
}

// Restore std::cout and std::cin
void restore_std_stream() {
  if (cout_buf) {
    std::cout.rdbuf(cout_buf);
    cout_buf = nullptr;
  }

  if (cin_buf) {
    std::cin.rdbuf(cin_buf);
    cin_buf = nullptr;
  }
}
