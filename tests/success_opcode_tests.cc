/*!
 * @file success_opcode_test.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Developer OpCode Test case (every time succeeded)
 */

#include <iostream>
#include <sstream>
#include "catch2/catch.hpp"
#include "helpers.h"
#include "opcode/add.h"
#include "opcode/dup.h"
#include "opcode/eq.h"
#include "opcode/gt.h"
#include "opcode/jmpz.h"
#include "opcode/lt.h"
#include "opcode/mul.h"
#include "opcode/opcode.h"
#include "opcode/pop.h"
#include "opcode/push.h"
#include "opcode/read.h"
#include "opcode/rot.h"
#include "opcode/sub.h"
#include "opcode/write.h"

using namespace opcode;

// Valid Test Case of all OpCode
TEST_CASE("ValidTest", "[OPCODE]") {
  SECTION("WRITE OpCode Usage") {
    opcode::WriteOpCode write_op;
    std::ostringstream out;
    redirect_std_stream(&out);
    opcode::ExecutionContext ctx;
    ctx.stack.push(1);

    try {
      // Execute test
      write_op.apply(ctx);
    } catch (...) {
      // Restaure previous status
      restore_std_stream();
      REQUIRE(false);
    }

    // Restaure previous status
    restore_std_stream();

    REQUIRE(out.str() == "1\n");
    REQUIRE(ctx.stack.empty());
  }
  //#--- End of WRITE OpCode Test

  SECTION("READ OpCode Usage") {
    opcode::ReadOpCode read_op;
    opcode::ExecutionContext ctx;

    // Replace and prepare input
    std::string give_number("1");
    std::istringstream in(give_number);
    std::ostringstream out;
    redirect_std_stream(&out, &in);

    try {
      // Execute test
      read_op.apply(ctx);
    } catch (...) {
      // Restaure previous status
      restore_std_stream();
      REQUIRE(false);
    }

    // Restaure previous status
    restore_std_stream();

    REQUIRE(!ctx.stack.empty());
    char *end;
    auto value = static_cast<int32_t>(std::strtol(give_number.data(), &end, 10));
    REQUIRE(ctx.stack.top() == value);
  }
  // #--- End of READ OpCode Test

  SECTION("Use DUP OpCode") {
    opcode::DupOpCode dup_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(3);

    // Execute test
    dup_op.apply(ctx);
    REQUIRE(ctx.stack.size() == 2);
    REQUIRE(ctx.stack.top() == 3);
    ctx.stack.pop();
    REQUIRE(ctx.stack.top() == 3);
  }
  // #--- End of DUP Test Case

  SECTION("Use MUL OpCode") {
    opcode::MulOpCode mul_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(3);
    ctx.stack.push(2);
    mul_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 6);
  }
  // #--- End of MUL Test Case

  SECTION("Use ADD OpCode") {
    opcode::AddOpCode add_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(3);
    ctx.stack.push(2);
    add_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 5);
  }
  // #--- End of ADD Test Case

  SECTION("Use SUB OpCode") {
    opcode::SubOpCode sub_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(2);
    ctx.stack.push(4);
    sub_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 2);
  }
  // #--- End of SUB Test Case
  SECTION("Use GT OpCode Greater") {
    opcode::GtOpCode gt_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(2);
    ctx.stack.push(3);
    gt_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 1);
  }
  // #--- End of GT Test Case
  SECTION("Use GT OpCode less") {
    opcode::GtOpCode gt_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(3);
    ctx.stack.push(2);
    gt_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 0);
  }
  // #--- End of GT Test Case
  SECTION("Use LT OpCode Greater") {
    opcode::LtOpCode lt_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(2);
    ctx.stack.push(3);
    lt_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 0);
  }
  // #--- End of LT Test Case
  SECTION("Use LT OpCode less") {
    opcode::LtOpCode lt_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(3);
    ctx.stack.push(2);
    lt_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 1);
  }
  // #--- End of LT Test Case
  SECTION("Use EQ OpCode Greater") {
    opcode::EqOpCode eq_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(3);
    ctx.stack.push(3);
    eq_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 1);
  }
  // #--- End of EQ Test Case
  SECTION("Use EQ OpCode less") {
    opcode::EqOpCode eq_op;
    opcode::ExecutionContext ctx;
    ctx.stack.push(2);
    ctx.stack.push(3);
    eq_op.apply(ctx);
    REQUIRE(ctx.stack.top() == 0);
  }
  // #--- End of EQ Test Case
  SECTION("Use JMPZ OpCode to jump") {
    opcode::JmpzOpCode opcode;
    opcode::ExecutionContext ctx;
    ctx.stack.push(0);
    ctx.stack.push(8);
    opcode.apply(ctx);
    REQUIRE(ctx.stack.empty());
    REQUIRE(ctx.next_opcode_idx == 8);
  }
  // #--- End of JMPZ Test Case
  SECTION("Use JMPZ OpCode to nop") {
    opcode::JmpzOpCode opcode;
    opcode::ExecutionContext ctx;
    ctx.stack.push(1);
    ctx.stack.push(8);
    opcode.apply(ctx);
    REQUIRE(ctx.stack.empty());
    REQUIRE(ctx.next_opcode_idx == 0);
  }
  // #--- End of JMPZ Test Case
  SECTION("Use PUSH OpCode") {
    opcode::PushOpCode opcode;
    opcode::ExecutionContext ctx;
    opcode.apply(ctx, OptionalParam(true, 7));
    REQUIRE(!ctx.stack.empty());
    REQUIRE(ctx.stack.top() == 7);
  }
  // #--- End of PUSH Test Case
  SECTION("Use POP OpCode") {
    opcode::PopOpCode opcode;
    opcode::ExecutionContext ctx;
    ctx.stack.push(1);
    ctx.stack.push(1);
    ctx.stack.push(1);
    opcode.apply(ctx, OptionalParam(true, 3));
    REQUIRE(ctx.stack.empty());
  }
  // #--- End of POP Test Case
  SECTION("Use ROT OpCode") {
    opcode::RotOpCode opcode;
    opcode::ExecutionContext ctx;
    ctx.stack.push(1);
    ctx.stack.push(2);
    ctx.stack.push(4);
    ctx.stack.push(8);
    opcode.apply(ctx, OptionalParam(true, 3));
    REQUIRE(!ctx.stack.empty());
    REQUIRE(ctx.stack.top() == 4);
    ctx.stack.pop();
    REQUIRE(ctx.stack.top() == 2);
    ctx.stack.pop();
    REQUIRE(ctx.stack.top() == 8);
    ctx.stack.pop();
    REQUIRE(ctx.stack.top() == 1);
    ctx.stack.pop();
  }
  // #--- End of ROT Test Case
}
// #--- End of Valid Test Case
