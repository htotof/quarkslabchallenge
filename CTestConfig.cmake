#--- Include Catch2
set(CATCH_DIR "${CMAKE_SOURCE_DIR}/third_party/Catch2")
add_subdirectory("${CATCH_DIR}")

# --- Include Catch2 CMake Modules
list(APPEND CMAKE_MODULE_PATH ${CATCH_DIR}/contrib)

# Other files
file(GLOB OPERAND_SRC ${CMAKE_SOURCE_DIR}/src/opcode/*.cc ${CMAKE_SOURCE_DIR}/src/opcode/*.h)
file(GLOB HELPERS_SRC ${CMAKE_SOURCE_DIR}/src/helpers/*.cc ${CMAKE_SOURCE_DIR}/src/helpers/*.h)
file(GLOB TESTCASE_SRC ${CMAKE_SOURCE_DIR}/tests/*.cc)

#--- Every library has unit tests, of course
add_executable(${PROJECT_NAME}_test
  ${TESTCASE_SRC}
  ${OPERAND_SRC} 
  ${HELPERS_SRC}
)

target_include_directories(${PROJECT_NAME}_test
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>   
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/tests>
    ${CATCH_DIR}/single_include
)

#--- C++17 support
target_link_libraries(${PROJECT_NAME}_test
    PRIVATE
        stdc++fs
)

#--- Get project output name for usage function
target_compile_definitions(${PROJECT_NAME}_test
    PRIVATE
      SAMPLE_ROOT="${CMAKE_CURRENT_SOURCE_DIR}/tests/samples"      
)

include(Catch)
catch_discover_tests(${PROJECT_NAME}_test)
