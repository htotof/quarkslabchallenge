/*!
 * @file console.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This is the console code for user input/output
 */

#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include "helpers/execute.h"

// Usage string
std::string usage();

// Main function
int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Parameters is missing:\n" << usage() << '\n';
    return EXIT_FAILURE;
  }

  std::string file_name(argv[1]);

  // Help is required
  if (file_name == "-h") {
    std::cout << usage() << '\n';
    return EXIT_SUCCESS;
  }

  // Read input file
  std::ifstream istrm(file_name);
  if (!istrm.is_open()) {
    std::cerr << "failed to open " << argv[1] << '\n';
    return EXIT_FAILURE;
  }
  return execute_code(istrm);
}
// #--- end of main

// Usage string
std::string usage() {
  std::ostringstream stream;
  stream << "Usage:\n\t" << APP_NAME << " [-h|ASSEMLBY_FILE]" << '\n'
         << "Program options:\n"
         << "\t-h\tDisplay help\n"
         << "\tASSEMBLY_FILE\tFile with code\n";
  return stream.str();
}
