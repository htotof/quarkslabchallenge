/*!
 * @file sub.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/sub.h"
#include <iostream>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Sub)

void SubOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (param.first) {
    throw std::runtime_error("SUB receive parameters but no need");
  }
  if (ctx.stack.size() < 2) {
    throw std::runtime_error("Failed to SUB opcode on too short stack size");
  }
  auto value1 = ctx.stack.top();
  ctx.stack.pop();
  auto value2 = ctx.stack.top();
  ctx.stack.pop();
  ctx.stack.push(value1 - value2);
}

};  // namespace opcode
//#--- End of namespace opcode
