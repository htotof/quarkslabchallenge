/*!
 * @file push.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/push.h"
#include <iostream>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Push)

void PushOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (!param.first) {
    throw std::runtime_error("PUSH opcode parameter is missing");
  }
  ctx.stack.push(param.second);
}

};  // namespace opcode
//#--- End of namespace opcode
