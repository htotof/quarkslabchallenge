/*!
 * @file pop.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * POP opcode: pop n value from the stack
 */
#ifndef OPCODE_POP_H
#define OPCODE_POP_H

#include "opcode/opcode.h"

namespace opcode {

class PopOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of PopOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_POP_H
