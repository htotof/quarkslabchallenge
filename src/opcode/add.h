/*!
 * @file add.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Add opcode: pops the two top value of the stack, add them and push the
 * result on top of the stack
 */
#ifndef OPCODE_ADD_H
#define OPCODE_ADD_H

#include "opcode/opcode.h"

namespace opcode {

class AddOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx, 
                OptionalParam param) override;
};  // #--- End of AddOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_ADD_H
