/*!
 * @file pop.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/pop.h"
#include <iostream>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Pop)

void PopOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (!param.first) {
    throw std::runtime_error("POP opcode parameter is missing");
  }

  if ((param.second < 0) || (static_cast<decltype(ctx.stack.size())>(
                                 param.second) > ctx.stack.size())) {
    throw std::runtime_error(
        "Invalid parameter to execute POP opcode: "
        "too short stack");
  }
  auto idx{param.second};
  while (idx > 0) {
    idx--;
    ctx.stack.pop();
  }
}

};  // namespace opcode
//#--- End of namespace opcode
