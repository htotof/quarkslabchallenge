/*!
 * @file jmpz.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/jmpz.h"
#include <iostream>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Jmpz)

/*!
 * pops the two top value of the stack. Jump to the <n>th instruction, where <n>
 * was the first value on the stack, if the top value is null. Otherwise just
 * drop these two values
 */
void JmpzOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (param.first) {
    throw std::runtime_error("JMPZ receive parameters but no need");
  }
  if (ctx.stack.size() < 2) {
    throw std::runtime_error("Failed to JMPZ opcode on too short stack size");
  }
  auto next_idx = ctx.stack.top();
  ctx.stack.pop();
  auto value = ctx.stack.top();
  ctx.stack.pop();
  if (value == 0) {
    if (next_idx < 0) {
      throw std::runtime_error("Invalid OpCode index (<0)");
    }
    ctx.next_opcode_idx = static_cast<uint32_t>(std::abs(next_idx));
  }
}

};  // namespace opcode
//#--- End of namespace opcode
