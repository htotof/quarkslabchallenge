/*!
 * @file mul.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * MUL opcode: pops the two top value of the stack, multiply them and push the
 * result on top of the stack
 */
#ifndef OPCODE_MUL_H
#define OPCODE_MUL_H

#include "opcode/opcode.h"

namespace opcode {

class MulOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of DupOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_MUL_H
