/*!
 * @file eq.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Eq opcode: pops the two top values from the stack, compare them for
 * TOP == SECOND and push the result as 0 or 1 on the stack
 */
#ifndef OPCODE_EQ_H
#define OPCODE_EQ_H

#include "opcode/opcode.h"

namespace opcode {

class EqOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of EqOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_EQ_H
