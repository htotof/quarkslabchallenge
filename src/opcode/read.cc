/*!
 * @file read.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/read.h"
#include <iostream>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Read)

void ReadOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (param.first) {
    throw std::runtime_error("READ receive parameters but no need");
  }
  std::cout << "Get Number (+/-) => ";

  std::string input;
  std::cin >> input;

  // No more number
  if (input.empty()) {
    throw StopExecution();
  }

  // Input validity
  bool is_first = true;
  int32_t coef{1};
  for (auto digit : input) {
    if (is_first) {
      is_first = false;
      if (digit == '-') {
        coef = -1;
        continue;
      }
      if (digit == '+') {
        coef = 1;
        continue;
      }
    }
    if (0 == std::isdigit(digit)) {
      std::ostringstream msg;
      msg << "Invalid input for '" << input << "'\n";
      throw std::invalid_argument(msg.str());
    }
  }
  char* end;
  auto str_in = static_cast<int32_t>(std::strtol(input.data(), &end, 10));
  ctx.stack.push(coef * str_in);
}

};  // namespace opcode
//#--- End of namespace opcode
