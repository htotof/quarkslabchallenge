/*!
 * @file sub.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Sub opcode: pops the two top value of the stack, sub the TOP with the SECOND
 * and push the result on the stack
 */
#ifndef OPCODE_SUB_H
#define OPCODE_SUB_H

#include "opcode/opcode.h"

namespace opcode {

class SubOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of SubOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_SUB_H
