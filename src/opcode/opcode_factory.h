/*!
 * @file opcode_factory.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Contains OpCode factory
 */
#ifndef OPCODE_OPCODE_FACTORY_H
#define OPCODE_OPCODE_FACTORY_H

#include <functional>
#include <map>
#include <queue>
#include <regex>
#include <stdexcept>

#include <algorithm>
#include <cctype>
#include <iostream>
#include "opcode/opcode.h"

namespace opcode {

class OpCodeFactory {
 private:
  using OpCodeMapType = std::map<std::string, OpCodePtr>;

 public:
  /*!
   * True if register is succesfull
   *
   * @param id OpCode ID
   * @param opcode_ptr callback creator function
   */
  bool register_opcode(const std::string &id, OpCodePtr opcode_ptr) {
    if (std::end(cb_) != cb_.find(id)) {
      throw std::runtime_error("Id already registered");
    }

    std::string upper_id{id};
    std::transform(std::begin(upper_id), std::end(upper_id),
                   std::begin(upper_id),
                   [](unsigned char c) { return std::toupper(c); });
    return (cb_.insert(OpCodeMapType::value_type(upper_id, opcode_ptr)).second);
  }

  // Create OpCode
  OpCodePtr get_opcode(const std::string &opcode_label) {
    auto iter = cb_.find(opcode_label);
    if (iter == std::end(cb_)) {
      throw std::runtime_error("Unknown Id");
    }

    return iter->second;
  }

 private:
  OpCodeMapType cb_;
};  //#--- End of OpCode factory
}  // #--- end of namespace opcode

#endif  // OPCODE_OPCODE_FACTORY_H
