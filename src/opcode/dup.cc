/*!
 * @file dup.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/dup.h"
#include <iostream>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Dup)

void DupOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (param.first) {
    throw std::runtime_error("DUP receive parameters but no need");
  }
  if (ctx.stack.empty()) {
    throw std::runtime_error("Failed to SUP opcode on empty stack");
  }
  auto value = ctx.stack.top();
  ctx.stack.push(value);
}

};  // namespace opcode
//#--- End of namespace opcode
