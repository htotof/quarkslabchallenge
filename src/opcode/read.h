/*!
 * @file read.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * READ opcode: reads an integer on stdin and push the value on the stack,
 * or exit if input is  invalid
 */
#ifndef OPCODE_READ_H
#define OPCODE_READ_H

#include "opcode/opcode.h"

namespace opcode {

class ReadOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of ReadOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_READ_H
