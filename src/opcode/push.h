/*!
 * @file push.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * PUSH opcode: push the integer value <n> on the stack
 */
#ifndef OPCODE_PUSH_H
#define OPCODE_PUSH_H

#include "opcode/opcode.h"

namespace opcode {

class PushOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of PushOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_PUSH_H
