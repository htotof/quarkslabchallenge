/*!
 * @file gt.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * GT Opcode: pops the two top values from the stack, compare them for
 * TOP > SECOND and push the result as 0 or 1 on the stack
 */
#ifndef OPCODE_GT_H
#define OPCODE_GT_H

#include "opcode/opcode.h"

namespace opcode {

class GtOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of GtOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_GT_H
