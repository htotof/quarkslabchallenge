/*!
 * @file jmpz.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * JMPZ OpCode: pops the two top value of the stack. Jump to the
 * <n>th instruction, where <n> was the first value on the stack, if the
 * top value is null. Otherwise just drop these two values
 */
#ifndef OPCODE_JMPZ_H
#define OPCODE_JMPZ_H

#include "opcode/opcode.h"

namespace opcode {

class JmpzOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of JmpzOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_JMPZ_H
