/*!
 * @file opcode_factory_mgr.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Make OpCodeFactory as singleton
 */
#ifndef OPCODE_FACTORY_MGR_H
#define OPCODE_FACTORY_MGR_H

#include "opcode/opcode_factory.h"

namespace opcode {

//-----------------------------------------------------------------------------
class OpCodeFactoryMgr {
 public:
  static OpCodeFactoryMgr &instance() {
    static OpCodeFactoryMgr instance;
    return instance;
  }

  /**
   * Root configuration accessor
   * @return the configuration
   */
  OpCodeFactory &get_opcode_factory() { return opcode_factory_; }

 private:
  OpCodeFactory opcode_factory_;
};  //#--- end of OpCodeFactoryMgr

}  // namespace opcode

#endif  // OPCODE_FACTORY_MGR_H
