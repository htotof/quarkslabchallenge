/*!
 * @file dup.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Dup opcode: duplicate the value on top of the stack
 */
#ifndef OPCODE_DUP_H
#define OPCODE_DUP_H

#include "opcode/opcode.h"

namespace opcode {

class DupOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of DupOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_DUP_H
