/*!
 * @file lt.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * LT opcode: pops the two top values from the stack, compare them for
 * TOP < SECOND and push the result as 0 or 1 on the stack
 */
#ifndef OPCODE_LT_H
#define OPCODE_LT_H

#include "opcode/opcode.h"

namespace opcode {

class LtOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of LtOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_LT_H
