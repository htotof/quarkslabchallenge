/*!
 * @file rot.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/rot.h"
#include <iostream>
#include <list>
#include <string>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Rot)

/*!
 * perform a circular rotation on the first n value of the stack toward
 * the top for instance the stack
 */
void RotOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (!param.first) {
    throw std::runtime_error("ROT opcode parameter is missing");
  }

  if ((param.second < 0) || (static_cast<decltype(ctx.stack.size())>(
                                 param.second) > ctx.stack.size())) {
    throw std::runtime_error(
        "Invalid parameter to execute ROT opcode: "
        "too short stack");
  }

  /*
   * Make rotation of rot_idx number:
   * ROT 3: BOTTOM [1,2,4,8] TOP => BOTTOM [1, 8, 2, 4] TOP
   */
  int32_t rot_idx{param.second};
  std::list<int32_t> tmp_buf;

  // Number set in front of group
  auto rot_value = ctx.stack.top();
  ctx.stack.pop();
  for (auto idx = rot_idx - 1; idx >= 1; --idx) {
    tmp_buf.push_front(ctx.stack.top());
    ctx.stack.pop();
  }
  tmp_buf.push_front(rot_value);

  // Pushd buffer to stack
  for (auto v : tmp_buf) {
    ctx.stack.push(v);
  }
}

};  // namespace opcode
//#--- End of namespace opcode
