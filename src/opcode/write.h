/*!
 * @file write.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Write opcode: pops the top value of the stack, and prints it on stdout
 */
#ifndef OPCODE_WRITE_H
#define OPCODE_WRITE_H

#include "opcode/opcode.h"

namespace opcode {

class WriteOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of WriteOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_WRITE_H
