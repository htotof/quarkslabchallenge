/*!
 * @file rot.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * ROT opcode: perform a circular rotation on the first n value of the stack
 * toward the top for instance the stack
 */
#ifndef OPCODE_ROT_H
#define OPCODE_ROT_H

#include "opcode/opcode.h"

namespace opcode {

class RotOpCode : public AbstractOpCode {
 public:
  // Apply operator
  void do_apply(ExecutionContext &ctx,
                OptionalParam param) override;
};  // #--- End of DupOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_ROT_H
