/*!
 * @file opcode.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Contains opcode definition and all other type around opcode type
 */
#ifndef OPCODE_OPERAND_H
#define OPCODE_OPERAND_H

#include <cstdint>
#include <iostream>
#include <memory>
#include <stack>

namespace opcode {

using NumberStack = std::stack<int32_t>;
using OpCodePtr = std::shared_ptr<class AbstractOpCode>;
using OptionalParam = std::pair<bool, int32_t>;

class StopExecution : public std::exception {};

struct ExecutionContext {
  NumberStack stack;
  uint32_t padding = 0;  // 4 bytes to alignment boundary
  uint32_t next_opcode_idx = 0;
};

// Abstract class that contains common code
class AbstractOpCode {
 public:
  /*!
   * Appy OpCode operation
   *
   * @param ctx Context execution
   * @param param parameter for OpCode
   *
   * @note NVI: Non-Virtual Interface
   */
  void apply(ExecutionContext &ctx,
             OptionalParam param = OptionalParam(false, 0)) {
    do_apply(ctx, param);
  }

  virtual ~AbstractOpCode() {}

 protected:
  /*!
   * Appy OpCode operation
   *
   * @param ctx Context execution
   * @param param parameter for OpCode
   */
  virtual void do_apply(ExecutionContext &ctx, OptionalParam param) = 0;
};  // #--- End of AbstractOpCode
}  // #--- end of namespace opcode

#endif  // OPCODE_OPERAND_H
