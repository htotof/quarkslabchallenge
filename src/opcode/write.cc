/*!
 * @file write.cc
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * This code need to be extract from header because of global static instance
 * use to register current opcode
 */

#include "opcode/write.h"
#include <iostream>
#include "opcode/opcode_register.h"

namespace opcode {

REGISTER_OPCODE(Write)

void WriteOpCode::do_apply(ExecutionContext &ctx, OptionalParam param) {
  if (param.first) {
    throw std::runtime_error("WRITE receive parameters but no need");
  }
  if (ctx.stack.empty()) {
    throw std::runtime_error("Failed to WRITE opcode on empty stack");
  }
  auto value = ctx.stack.top();
  ctx.stack.pop();
  std::cout << value << '\n';
}

};  // namespace opcode
//#--- End of namespace opcode
