/*!
 * @file opcode_register.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Exported macro to compile time register opcode
 */
#ifndef OPCODE_OPCODE_REGISTER_H
#define OPCODE_OPCODE_REGISTER_H

#include "opcode/opcode_factory_mgr.h"

namespace opcode {

#define STRINGS(s) #s

#ifndef REGISTER_OPCODE
/*!
 * @param className (Class declaration) Class Name With Find function
 */
#define REGISTER_OPCODE(className)                                      \
  namespace {															\
  auto Registered##className =                                     	  	\
      	OpCodeFactoryMgr::instance().get_opcode_factory().register_opcode(\
          std::string(STRINGS(className)),                                \
          std::make_shared<className##OpCode>());}
#endif
}  // #--- end of namespace opcode

#endif  // OPCODE_OPCODE_REGISTER_H
