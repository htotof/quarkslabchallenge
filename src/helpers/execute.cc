#include "execute.h"
#include "convert.h"

#include "helpers/convert.h"
#include "opcode/opcode.h"
#include "opcode/opcode_factory_mgr.h"

/*!
 * @brief convert and execute code
 * @param opcode_list file contains
 * @return execution return code
 */
int execute_code(std::istream &opcode_list) {
  using helpers::get_opcode_from_string;
  using opcode::ExecutionContext;
  using opcode::OpCodeFactoryMgr;

  ExecutionContext execution_ctx;

  // Convert
  for (std::string line; std::getline(opcode_list, line);) {
    // Stop is required
    if (line.empty()) {
      std::cout << "Execution stopped by user\n";
      return EXIT_SUCCESS;
    }
    try {
      // Translate the readed line
      uint32_t cur_idx;
      std::string opcode;
      std::pair<bool, int32_t> parameter;
      std::tie(cur_idx, opcode, parameter) = get_opcode_from_string(line);

      // First check
      if (cur_idx != execution_ctx.next_opcode_idx) {
        continue;
      }

      // Valid index command
      ++execution_ctx.next_opcode_idx;

      // Get opcode object
      auto opcode_obj =
          OpCodeFactoryMgr::instance().get_opcode_factory().get_opcode(opcode);

      // Execute code
      opcode_obj->apply(execution_ctx, parameter);

    } catch (opcode::StopExecution &) {
      std::cout << "Execution stopped by user\n";
      return EXIT_SUCCESS;
    } catch (std::exception &err) {
      std::cerr << "Error on '" << line << "': " << err.what() << '\n';
      return EXIT_FAILURE;
    }
  }

  std::cout << "Execute all commands with success\n";
  return EXIT_SUCCESS;
}
// #--- end of execute_code
