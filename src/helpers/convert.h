/*!
 * @file convert.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Convert and check string to be convert to operands
 */
#ifndef HELPERS_CONVERT_H
#define HELPERS_CONVERT_H

#include <string>
#include <tuple>
#include "opcode/opcode.h"

namespace helpers {

enum class EOValue : int16_t { index = 0, opcode = 1, optional_param = 2 };

using ExtractedOperand =
    std::tuple<uint32_t, std::string, opcode::OptionalParam>;

/*!
 * @brief idx_operand_param
 * @return extracted data
 */
ExtractedOperand get_opcode_from_string(const std::string &idx_operand_param);

}  // #--- End of namespace helpers

#endif  // HELPERS_CONVERT_H
