#include "helpers/convert.h"
#include <exception>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

namespace helpers {

/*!
 * @brief read file contains
 * @param file_path file path
 * @return file contains
 */
ExtractedOperand get_opcode_from_string(const std::string &idx_operand_param) {
  static std::regex operand_regex("([0-9]{1,}) ([A-Z]*)(?: (-?\\d*))?");
  std::smatch base_match;

  if (!std::regex_match(idx_operand_param, base_match, operand_regex)) {
    std::ostringstream stream;
    stream << "Invalid operand syntax =>'" << idx_operand_param << "'\n";
    throw std::runtime_error(stream.str());
  }

  // Collect data
  char *end;
  int32_t opcode_idx = 
      static_cast<int32_t>(std::strtol(base_match[1].str().c_str(), &end, 10));
  if (base_match.size() == 4 && base_match[3].str().empty()) {
    return std::make_tuple(opcode_idx, base_match[2].str(),
                           std::make_pair<bool, int32_t>(false, 0));
  }

  if (base_match.size() == 4) {
    int32_t param_value = 
      static_cast<int32_t>(std::strtol(base_match[3].str().c_str(), &end, 10));
    return std::make_tuple(opcode_idx, base_match[2].str(),
                           std::make_pair<bool, int32_t>(true, 
                                                        std::move(param_value)));
  }

  throw std::runtime_error("Invalid input");
}
// #--- End of get_opcode_from_string
};  // namespace helpers
    // #--- End of namespace helpers
