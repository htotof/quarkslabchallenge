/*!
 * @file execute.h
 * @author HOYON Christophe [htotof@gmail.com]
 *
 * Read input stream then execute identified OpCode
 */
#ifndef HELPERS_EXECUTE_H
#define HELPERS_EXECUTE_H

#include <iostream>

/*!
 * Read line like "IDX OPCODE PARAM" line by line then execute operation.
 *
 * Stop processing when input emit empty string or EOF
 */
int execute_code(std::istream &opcode_list);

#endif  //#define HELPERS_EXECUTE_H
