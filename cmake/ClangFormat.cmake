# Required clang-format
# find . -name '*.cc' -or -name '.h' | xargs clang-format -i style=Google $1

# Get all files
file(GLOB_RECURSE ALL_SOURCE_FILES *.cc *.h)

# Execute command
add_custom_target(
    clangformat
    COMMAND /usr/bin/clang-format
    -style=Google
    -i
    ${ALL_SOURCE_FILES}
)