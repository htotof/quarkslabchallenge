# Required clang-tidy
find_program(
    CLANG_TIDY_EXE
    NAMES "clang-tidy"
    DOC "Path to clang-tidy executable"
)

if (NOT CLANG_TIDY_EXE)
    MESSAGE(STATUS "clang-tidy not found.")
else()
    MESSAGE(STATUS "clang-tidy found ${CLANG_TIDY_EXE}.")
    
    #
    # define parameters
    #
    set(CLANG_TIDY_PARAM "-checks=*,google-*, modernize-*")
    # Authorize default parameter for function
    set(CLANG_TIDY_PARAM "${CLANG_TIDY_PARAM}, -fuchsia-default-arguments")
    # 'span' not yet in standard library => argv cannot be convert to string
    set(CLANG_TIDY_PARAM "${CLANG_TIDY_PARAM}, -cppcoreguidelines-pro-bounds-pointer-arithmetic")
    # Remove LLVM Coding style option: we are using Google Coding style but still presist
    set(CLANG_TIDY_PARAM "${CLANG_TIDY_PARAM}, -llvm-include-order")
    # Authorized static global variable: for auto-register method mandatory
    set(CLANG_TIDY_PARAM "${CLANG_TIDY_PARAM}, -cert-err58-cpp")

    # define command
    set(DO_CLANG_TIDY "${CLANG_TIDY_EXE}" "${CLANG_TIDY_PARAM}")
endif()
